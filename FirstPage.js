// function signUp() {
//     var firstName = $('#firstName').val();
//     var lastName = $('#lastName').val();
//     var email = $('#email').val();
//     var password = $('#password').val();
//     data = {
//         firstName: firstName,
//         lastName: lastName,
//         email: email,
//         password: password
//     }
//     postbackend(data);
// }

// function postbackend(data) {
//     $.ajax({
//         url: URI.BASE_URL + "users.json",
//         // url: "https://kalculate-f5d42.firebaseio.com/users.json",
//         data: JSON.stringify(data),
//         type: 'POST',
//         dataType: 'json',
//         success: function (response) {
//             console.log('done');
//             $("#successMessage").text("Sign Up Successfull !!")
//         },
//         error: function (error) {
//             console.log('error');
//         }
//     })
// }

// function getUsersFromBackend(UserData) {
//     $.ajax({
//         url: URI.BASE_URL + "users.json",
//         // url: "https://kalculate-f5d42.firebaseio.com/users.json",
//         type: 'GET',
//         contentType: 'text/plain',
//         dataType: 'json',
//         success: function (data) {
//             checkLogin(data, UserData);

//         },
//         error: function (error) {
//             console.log(error)
//         }
//     });
// }

// function checkLogin(data, UserData) {
//     // console.log(Object.keys(data));
//     var counterPlus = 0;
//     Object.keys(data).forEach(key => {
//         let user = data[key];
//         if (user.email === UserData.emailUser && user.password === UserData.passwordUser) {
//             console.log("login done");
//             counterPlus = 1;
//         }
//     });
//     if (counterPlus == 1) {
//         $("#errorMessage").text("");
//         // window.open("TemplateBuilder.html","_parent");
//         // window.open("test.html", "_parent");
//         // $("#errorMessage").text("Login Successfull !!");

//         $("#tryThis").click();
//         document.getElementById("asideBar").style.display = "block";

//     } else {
//         $("#errorMessage").text("Invalid Credentials !!");
//     }
// }

// function signIn() {
//     var emailUser = $('#emailUser').val();
//     var passwordUser = $('#passwordUser').val();

//     UserData = {
//         emailUser: emailUser,
//         passwordUser: passwordUser
//     }
//     console.log(UserData);
//     getUsersFromBackend(UserData);
// }

//authentication
var config = {
    apiKey: "AIzaSyC5F_rVxOM0RiIBUeKyE4fWto3OwuJL4DY",
    authDomain: "kalculate-f5d42.firebaseapp.com",
    databaseURL: "https://kalculate-f5d42.firebaseio.com",
    projectId: "kalculate-f5d42",
    storageBucket: "kalculate-f5d42.appspot.com",
    messagingSenderId: "753822206781"
};
firebase.initializeApp(config);
var database = firebase.database();




//ADD LOGIN EVENT
function try_login() {
    const emailUser = $('#emailUser').val();
    const passwordUser = $('#passwordUser').val();
    const auth = firebase.auth();
    const promise = auth.signInWithEmailAndPassword(emailUser, passwordUser);
    promise.catch(e => console.log(e.message));
}
function try_signUp() {
    var email = $('#email').val();
    var password = $('#password').val();
    const auth = firebase.auth();
    const promise = auth.createUserWithEmailAndPassword(email, password);
    $("#dismiss").click();
    promise.catch(e => {
        console.log(e.message);
        // $('#successMessage').text(e);
    });

    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var userObject = JSON.parse(localStorage.getItem("userObject"));
    var details = {
        firstName: firstName,
        lastName: lastName
    };
    firebase.database().ref("/users/" + userObject["uid"] + "/").set(details);
    console.log(userObject["uid"]);
}

function logout() {
    firebase.auth().signOut();
    document.getElementById('nav3').style.display = "none";
    document.getElementById('nav1').style.display = "block";
    document.getElementById('nav2').style.display = "block";
    document.getElementById("asideBar").style.display = "none";
}

firebase.auth().onAuthStateChanged(firebaseUser => {
    if (firebaseUser) {
        // var displayName = firebaseUser.displayName;
        var email = firebaseUser.email;
        var uid = firebaseUser.uid;

        var obj = {
            uid: uid,
            email: email
        };
        localStorage.setItem("userObject", JSON.stringify(obj));
        console.log(firebaseUser);
        console.log("user logged in");
        $("#tryThis").click();
        document.getElementById("asideBar").style.display = "block";
        document.getElementById('nav3').style.display = "block";
        document.getElementById('nav1').style.display = "none";
        document.getElementById('nav2').style.display = "none";
    } else {
        console.log("not logged in");
    }
});

// FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();